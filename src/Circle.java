public class Circle extends GeometryFigure{
    private double radius;
    private final double PI = 3.14;

    public double getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    double getSquare() {
        return radius * radius * PI;
    }
}
