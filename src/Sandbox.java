public class Sandbox {
    public static void main(String[] args) {
        GeometryFigure[] figures = new GeometryFigure[4];
        figures[0] = new Circle(5);
        figures[1] = new Rectangle(12, 6);
        figures[2] = new Square(5);
        figures[3] = new RectangularTrapezoid(5, 3, 5);
        for(GeometryFigure figure: figures) {
            System.out.println(figure.getSquare());
        }
    }
}
