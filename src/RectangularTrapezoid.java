public class RectangularTrapezoid extends GeometryFigure{
    private double height;
    private double side1;
    private double side2;

    @Override
    double getSquare() {
        return (side1 + side2) * height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getSide1() {
        return side1;
    }

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public double getSide2() {
        return side2;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    public RectangularTrapezoid(double height, double side1, double side2) {
        this.height = height;
        this.side1 = side1;
        this.side2 = side2;
    }
}
